## Pohi - Influencer marketplace https://www.pohi.io/#/
Pohi on turuplats kus ärid saavad otse YouTube ja Twitch mõjutajatelt osta sisuturundusteenust. Pohi on täielikult live ning ka igapäevases kasutuses. 

## English version - Pohi
Pohi is a marketplace where platform where companies can directly hire YouTube and Twitch influencers for product endorsement purposes. Pohi is currently in production.

Pohi is built on the MEAN stack.

![alt text](https://www.pohi.io/assets/images/creatordash.png)

## Venturo - Veebipõhine äritarkvara
Venturo on väikeäridele suunatud ärihaldustarkvara. Venturo ei ole täiesti "live", funktsionaalsus töötab ning lehte saab kasutada siin: http://venturo.herokuapp.com/

Venturo backend on loodud NodeJS põhjal ning andmebaasina kasutab MongoDB-d.

### English version - Venturo
Venturo is a business management SAAS application targeted at small businesses. The application is not in production, but is functional. http://venturo.herokuapp.com/

Venturo uses NodeJS on the backend and MongoDB for data storage.

![alt text](https://i.imgur.com/UhmJYvs.png)


## Pythoniga programmeeritud mäng
Mängu idee on koguda punkte ning hävitada 2D zombisid. Mäng on loodud programmeerimise alused aine raames, kasutab pygame enginet.

### English version - 2D python game
The aim of the game is to exterminate 2D zombies, it was created for my "introduction to programming" course at Tartu University. The game utilizes pygame engine for rendering sprites. 

![alt text](https://i.imgur.com/2je0yxl.png)